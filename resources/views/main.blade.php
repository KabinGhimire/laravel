  @section('main')
<!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
          <a href="post.html">
            <h2 class="post-title">
              It is always the inner
feeling of inferiority that makes one aggressive and violent.
            </h2>
            <h3 class="post-subtitle">
              When one thinks of attacking and defeating another, it means one has already accepted one’s
inferiority before the other. One who is really strong and great cannot think of fighting and subduing
anyone, because he does not find himself inferior to another in any manner. He does not need to
defeat someone to buttress his self confidence; he is sufficient unto himself.
it, he already has it.
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#">Admin</a>
            on , 2018</p>
        </div>
        <hr>
        <div class="post-preview">
          <a href="post.html">
            <h2 class="post-title">
              The desire to win, to conquer, is a later development in the life of man, when his mind
  is diseased.
            </h2>
          </a>
          <p class="post-meta">Posted by
            <a href="#">ADMIN</a>
            on , 2018</p>
        </div>
        <hr>


        <div class="post-preview">
          <a href="post.html">
            <h2 class="post-title">
              Failure is not an option
            </h2>
            <h3 class="post-subtitle">
              Killing does not destroy the wicked.
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#">Admin</a>
            on  2018</p>
        </div>
            <hr>
             <!-- Pager -->
        <div class="clearfix">
       <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
     </div>
   </div>
 </div>
</div>

<hr>


        @endsection
