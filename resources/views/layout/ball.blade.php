<!DOCTYPE html>
<html lang="en">
  <title>Clean Blog - Start Bootstrap Theme</title>
  <head>


    <meta charset="utf-8">
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

    <link rel="stylesheet" href="css/signin.css" >

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">



      <!-- Bootstrap core CSS -->


      <link href="css/all.css" rel="stylesheet">

      <link href="css/bootstra.css" rel="stylesheet">
      <link href="css/fontsass.css" rel="stylesheet">
      <link href="css/scss.css" rel="stylesheet">
      <link href="css/less.css" rel="stylesheet">

      <!-- Custom styles for this template -->
    <!--  <link href="css/simple-sidebar.css" rel="stylesheet"> -->
     <link href="css/font.css" rel="stylesheet" type="text/css">

   <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
     <link href="css/clean.css" rel="stylesheet">


  </head>

  <body>
    @section('content')

         <div class="content">
         <div id="wrapper">
           @include('inc.wrapper')
           @yield('wrapper')
           @include('content.content')
           @yield('info')
           @include('navbar')
           @yield('navigation')
           @include('header')
           @yield('header')
           @include('main')
           @yield('main')


</div>


  @show
  </div>

      <!-- /#wrapper -->

      <!-- Bootstrap core JavaScript -->
      <script src="js/all.js"></script>

      <script src="js/cleanjs.js"></script>
       <script  src="js/login1.js"></script>
         <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
      <!--<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
      <!-- Menu Toggle Script -->
      <script>
      $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");

      });
      </script>
      <script>
      $("#menu").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");

      });
      </script>






  </body>
  <footer>

    @include('footer')
    @yield('footer')


  </footer>

  </html>
